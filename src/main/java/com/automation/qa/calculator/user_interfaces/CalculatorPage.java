/*
 * Copyright (c) 2021
 * David Moreno Hernandez - Test automation
 * https://www.linkedin.com/public-profile/in/davidh-morenoh/
 *
 * All rights reserved Date: 08/01/21, 7:39 p. m.
 */

package com.automation.qa.calculator.user_interfaces;

import net.serenitybdd.screenplay.targets.Target;

/**
 * @author <a href="davidh.morenoh@outlook.com">David Moreno Hernandez</a>
 * @version 1.0.0
 * @since 1.0.0
 **/
public class CalculatorPage {

    public static final Target RESULT = Target.the("result").locatedBy("//span[@id='cwos']");
    public static final Target NUMBER = Target.the("{0}").locatedBy("//div[text()='{0}']");
    public static final Target OPERATION = Target.the("{0}").locatedBy("//div[@aria-label='{0}']");
    public static final Target EQUAL = Target.the("equal").locatedBy("//div[@aria-label='igual']");
    public static final Target PI = Target.the("pi").locatedBy("//div[contains(text(),'π')]");

    private CalculatorPage() {
    }

}