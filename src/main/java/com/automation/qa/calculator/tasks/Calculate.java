package com.automation.qa.calculator.tasks;

import com.automation.qa.calculator.interactions.PushKey;
import com.automation.qa.calculator.interactions.SelectOperation;
import com.automation.qa.calculator.user_interfaces.CalculatorPage;
import com.openhtmltopdf.extend.UserInterface;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;

import java.util.List;

public class Calculate implements Task {

    private String figura;
    private String operacion;
    private String paramUno;
    private String paramDos;

    public Calculate(String figura, String paramUno,String paramDos) {
        this.figura = figura;
        this.paramUno = paramUno;
        this.paramDos = paramDos;

    }


    @Override
    public <T extends Actor> void performAs(T actor) {

        if (operacion.equalsIgnoreCase("volumen")) {
            if (figura.equalsIgnoreCase("Cilindro")) {
                actor.attemptsTo(Click.on(CalculatorPage.PI),
                        SelectOperation.arithmetic("multiplicacion"),
                        PushKey.number(paramUno),
                        SelectOperation.arithmetic("multiplicacion"),
                        PushKey.number(paramUno),
                        SelectOperation.arithmetic("multiplicacion"),
                        PushKey.number(paramDos),
                        Click.on(CalculatorPage.EQUAL));
            }
        }
    }
    public static Calculate toFigure (String figura,String paramUno,String paramDos){


        return Tasks.instrumented(Calculate.class, figura, paramUno,paramDos);
    }


    public Calculate operation (String operation){

        this.operacion = operation;
        return this;

    }
}
