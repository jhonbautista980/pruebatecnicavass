package com.automation.qa.calculator.tasks;

import com.automation.qa.calculator.interactions.PushKey;
import com.automation.qa.calculator.interactions.SelectOperation;
import com.automation.qa.calculator.user_interfaces.CalculatorPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;

public class CalcularAceleracion implements Task {

    private String operacion;
    private String paramUno;
    private String paramDos;
    private String MIL = "1000";
    public CalcularAceleracion(String operacion) {

        this.operacion = operacion;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        if (operacion.equalsIgnoreCase("acelaracion")) {
            // Se crea una conts para que haga la conversion a Metros sobre segundos al cuadrado

            actor.attemptsTo(PushKey.number(paramUno),
                    SelectOperation.arithmetic("division"),
                    PushKey.number(paramDos),
                    Click.on(CalculatorPage.EQUAL),
                    SelectOperation.arithmetic("multiplicacion"),
                    PushKey.number(MIL),
                    Click.on(CalculatorPage.EQUAL)
                    );

        }
    }

    public static CalcularAceleracion DoCalculo (String operacion){

        return Tasks.instrumented(CalcularAceleracion.class, operacion);
    }

    public CalcularAceleracion withTwoParams (String paramUno, String paramDos){
        this.paramUno = paramUno;
        this.paramDos = paramDos;
        return this;
    }
}
