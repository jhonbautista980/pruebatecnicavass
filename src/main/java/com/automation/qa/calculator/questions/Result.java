/*
 * Copyright (c) 2021
 * David Moreno Hernandez - Test automation
 * https://www.linkedin.com/public-profile/in/davidh-morenoh/
 *
 * All rights reserved Date: 08/01/21, 7:22 p. m.
 */

package com.automation.qa.calculator.questions;

import com.automation.qa.calculator.user_interfaces.CalculatorPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

/**
 * @author <a href="davidh.morenoh@outlook.com">David Moreno Hernandez</a>
 * @version 1.0.0
 * @since 1.0.0
 **/
public class Result implements Question<String> {

    @Override
    public String answeredBy(Actor actor) {

        return Text.of(CalculatorPage.RESULT).viewedBy(actor).asString();
    }

    public static Result is() {
        return new Result();
    }

}