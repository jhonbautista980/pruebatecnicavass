/*
 * Copyright (c) 2021
 * David Moreno Hernandez - Test automation
 * https://www.linkedin.com/public-profile/in/davidh-morenoh/
 *
 * All rights reserved Date: 08/01/21, 7:17 p. m.
 */

package com.automation.qa.calculator.interactions;

import com.automation.qa.calculator.user_interfaces.CalculatorPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;

/**
 * @author <a href="davidh.morenoh@outlook.com">David Moreno Hernandez</a>
 * @version 1.0.0
 * @since 1.0.0
 **/
public class PushKey implements Interaction {

    private String value;

    public PushKey(String value) {

        this.value = value;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        for (int i = 0; i < value.length(); i++) {
            actor.attemptsTo(Click.on(CalculatorPage.NUMBER.of(String.valueOf(value.charAt(i)))));
        }
    }

    public static PushKey number(String value) {
        return Tasks.instrumented(PushKey.class, value);
    }

}