package com.automation.qa.calculator.step_definitions;

import com.automation.qa.calculator.questions.Result;
import com.automation.qa.calculator.tasks.Calculate;
import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.actors.OnStage;

import java.util.List;

public class CalcularVolumenCilindroSteps {

    @When("^calcular (.*) del (.*) con un (.*) y (.*)$")
    public void calcularOperacionALaFigura
            (String calculo, String figura,
             String paramUno, String paramDos) {
        OnStage.theActorInTheSpotlight().attemptsTo(Calculate.
                toFigure(figura, paramUno,paramDos).operation(calculo));

    }

}
