/*
 * Copyright (c) 2021
 * David Moreno Hernandez - Test automation
 * https://www.linkedin.com/public-profile/in/davidh-morenoh/
 *
 * All rights reserved Date: 08/01/21, 7:44 p. m.
 */

package com.automation.qa.calculator.step_definitions;

import com.automation.qa.calculator.drivers.OwnWebDriver;
import com.automation.qa.calculator.questions.Result;
import com.automation.qa.calculator.tasks.DoOperation;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actors.Cast;
import net.serenitybdd.screenplay.actors.OnStage;
import org.hamcrest.Matchers;

/**
 * @author <a href="davidh.morenoh@outlook.com">David Moreno Hernandez</a>
 * @version 1.0.0
 * @since 1.0.0
 **/
public class CalculatorSteps {

    @Given("^Abro la calculadora de Google$")
    public void open_the_Google_calculator() {
        OnStage.setTheStage(Cast.ofStandardActors());
        OnStage.theActorCalled("User");
        OnStage.theActorInTheSpotlight().can(BrowseTheWeb
                .with(OwnWebDriver.withChrome().setURL("https://www.google.com/search?q=calculadora+google")));



    }

    @When("^Realizo la (.*) de (.*) y (.*)$")
    public void do_operation(String operation, String numberOne, String numberTwo) {
        OnStage.theActorInTheSpotlight().attemptsTo(DoOperation.arithmetic(operation).withTwoNumbers(numberOne, numberTwo));
    }

    @Then("^El resultado debe ser (.*)$")
    public void result_is(String result) {
        OnStage.theActorInTheSpotlight().should(GivenWhenThen.seeThat(Result.is(), Matchers.equalTo(result)));
    }

}