package com.automation.qa.calculator.step_definitions;

import com.automation.qa.calculator.tasks.CalcularAceleracion;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.actors.OnStage;

public class CalcularAceleracionSteps {


    @When("^Realizo la (.*) con un (.*) y (.*)$")
    public void calcularOperacionALaFigura(String aceleracion, String paramUno, String paramDos) {
            OnStage.theActorInTheSpotlight().attemptsTo
                    (CalcularAceleracion.DoCalculo(aceleracion).withTwoParams(paramUno,paramDos));
    }
}
