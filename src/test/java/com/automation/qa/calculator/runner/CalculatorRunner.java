/*
 * Copyright (c) 2021
 * David Moreno Hernandez - Test automation
 * https://www.linkedin.com/public-profile/in/davidh-morenoh/
 *
 * All rights reserved Date: 08/01/21, 7:42 p. m.
 */

package com.automation.qa.calculator.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

/**
 * @author <a href="davidh.morenoh@outlook.com">David Moreno Hernandez</a>
 * @version 1.0.0
 * @since 1.0.0
 **/
@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features = "src/test/resources/features/Calculator.feature",
        snippets = SnippetType.CAMELCASE,
        glue = "com.automation.qa.calculator.step_definitions"
//        ,tags = "@Suma" run scenario or feature with tag
)
public class CalculatorRunner {
}