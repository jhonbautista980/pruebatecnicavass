package com.automation.qa.calculator.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features = "src/test/resources/features/CalcularVolumenCilindro.feature",
        glue = "com.automation.qa.calculator.step_definitions",
        snippets = SnippetType.CAMELCASE
)
public class CalcularVolumenCilindroRunner {


}
