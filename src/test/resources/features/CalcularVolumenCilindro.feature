Feature: Calcular volumen de una figura
  Yo como usuario de la calculadora de Google
  Quiero Realizar el calculo del volumen de una figura
  Para poder calcular el volumen de una figura


  Scenario Outline: Calcular volumen de un cilindro
    Given Abro la calculadora de Google
    When calcular volumen del Cilindro con un <paramUno> y <paramDos>
    Then El resultado debe ser 183368.480005


    Examples:
      | paramUno | paramDos |
      | 32       | 57       |