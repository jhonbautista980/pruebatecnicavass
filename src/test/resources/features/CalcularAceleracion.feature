Feature: Calcular las formulas de movimiento de un objeto
  Yo como usuario de la calculadora de Google
  Quiero realizar el calculo de acelaracion
  Para poder calcular su resultado

  Scenario Outline: Calcular formulas de movimiento  de Un Avion
    Given Abro la calculadora de Google
    When Realizo la acelaracion con un <paramUno> y <paramDos>
    Then El resultado debe ser 5400


    Examples:
      | paramUno | paramDos |
      | 432      | 80       |