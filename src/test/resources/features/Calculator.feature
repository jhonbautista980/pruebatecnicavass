Feature: Realizar operaciones matematicas
  Yo como usuario de la calculadora de Google
  Quiero realizar las operaciones aritmeticas de suma, resta, multiplicacion y division
  Para poder calcular cualquier resultado

  @Sum
  Scenario: Sumar dos numeros naturales positivos
    Given Abro la calculadora de Google
    When Realizo la suma de 2342354235 y 9125323523
    Then El resultado debe ser 11467677758

  @Subtraction
  Scenario: Restar dos numeros naturales positivos
    Given Abro la calculadora de Google
    When Realizo la resta de 14 y 8
    Then El resultado debe ser 6

  @Multiplication
  Scenario: Multiplicar dos numeros naturales positivos
    Given Abro la calculadora de Google
    When Realizo la multiplicacion de 8 y 8
    Then El resultado debe ser 64

  @Division
  Scenario: Dividir dos numeros naturales positivos
    Given Abro la calculadora de Google
    When Realizo la division de 64 y 8
    Then El resultado debe ser 8